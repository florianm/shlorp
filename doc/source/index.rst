.. Shlorp documentation master file, created by
   sphinx-quickstart on Tue Apr 29 17:25:09 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Shlorp's documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 2

Fabric build script
===================
All operations are managed via the fabric build script.

Execute any function as `fab FUNCTION`, e.g. `fab setup` to get started.

List all available functions with `fab -l`.

.. automodule:: fabfile
   :members:

shlorp.settings
===============
To get started, copy shlorp/settings.template to shlorp/settings.py and modify its variables.

.. automodule:: shlorp.settings
   :members:

shlorp.convert
==============
The workhorse of this package is shlorp.convert, containing all file handling operations.

.. automodule:: shlorp.convert
   :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

